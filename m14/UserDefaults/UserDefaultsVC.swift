import UIKit

class UserDefaultsVC: UIViewController {

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var surname: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        name.text = UserData.shared.userName ?? ""
        surname.text = UserData.shared.userSurName ?? ""
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func nameChanged(_ sender: Any) {
        UserData.shared.userName = name.text!
    }
    
    @IBAction func surnameChanged(_ sender: Any) {
        UserData.shared.userSurName = surname.text!
    }

}
