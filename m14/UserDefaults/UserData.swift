import Foundation

class UserData {
    static let shared = UserData()
    
    private let kUserNameKey = "UserData.kUserNameKey"
    private let kUserSurnameKey = "UserData.kUserSurnameKey"
    
    var userName: String? {
        set { UserDefaults.standard.set(newValue, forKey: kUserNameKey) }
        get { return UserDefaults.standard.string(forKey: kUserNameKey) }
    }
    
    var userSurName: String? {
        set { UserDefaults.standard.set(newValue, forKey: kUserSurnameKey) }
        get { return UserDefaults.standard.string(forKey: kUserSurnameKey) }
    }
    
}
