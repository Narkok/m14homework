import UIKit

class ForecastCell: UICollectionViewCell {
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var hour: UILabel!
    @IBOutlet weak var title: UILabel!
}
