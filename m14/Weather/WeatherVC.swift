import UIKit

class WeatherVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var city: String = "Moscow"
    var weather: Weather!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        weather = Weather(in: city)
        weather.delegate = self
        weather.update()
    }
}

extension WeatherVC: WeatherDelegate {
    func updated(message: String, sender: Weather) {
        collectionView.reloadData()
    }
}

extension WeatherVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return weather.newWeatherloaded ? weather.forecast.count : weather.savedForecast.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "WeatherHeader", for: indexPath) as! ForecastHeader
        
        if weather.newWeatherloaded {
            headerView.city.text = weather.city
            headerView.temp.text = String(weather.weather) + "˚"
        }
        else {
            headerView.city.text = weather.savedCity
            headerView.temp.text = String(weather.savedWeather) + "˚"

        }
        
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForecastCell", for: indexPath) as! ForecastCell
        
        let hour = 3 * (indexPath.row % 8)
        var time = String(hour) + ":00"
        if (hour < 10) { time = "0" + time }
        
        let forecast = weather.newWeatherloaded ? weather.forecast[indexPath.row] : weather.savedForecast[indexPath.row]
        
        let temp = forecast.temp
        
        cell.title.text = (indexPath.row % 8 == 0) ? forecast.title : ""
        cell.hour.text = time
        cell.temp.text = (temp == nil) ? "—" : String(temp!) + "˚"
        
        return cell
    }
}

extension WeatherVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = Int(self.collectionView.frame.width / 8)
        return CGSize(width: width, height: 85)
    }
}
