import UIKit

class ForecastHeader: UICollectionViewCell {
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var temp: UILabel!
}
