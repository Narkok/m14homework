import Foundation
import Alamofire
import RealmSwift

protocol WeatherDelegate {
    func updated(message: String, sender: Weather)
}

class ForecastItemRealm: Object {
    @objc dynamic var title: String = ""
    @objc dynamic var temp: Int = 0
    
    func new(_ title: String, _ temp: Int?) -> ForecastItemRealm {
        self.title = title
        self.temp = temp ?? -1000
        return self
    }
}

class CurrentWeatherItemRealm: Object {
    @objc dynamic var city: String = ""
    @objc dynamic var temp: Int = 0
}

class Weather {
    
    var delegate: WeatherDelegate?
    
    var city: String = ""
    var weather: Int = 0
    var forecast: [(title: String, temp: Int?)] = []
    
    var newWeatherloaded = false
    
    var savedCity: String = ""
    var savedWeather: Int = 0
    var savedForecast: [(title: String, temp: Int?)] = []
    
    private var weatherUpdated: Bool = false
    private var forecastUpdated: Bool = false
    
    init(in city: String) {
        self.newWeatherloaded = false
        self.city = city
        getSavedWeather()
    }
    
    func getSavedWeather() {
        let realm = try! Realm()
        
        let savedForecast = realm.objects(ForecastItemRealm.self)
        var _forecast: [(title: String, temp: Int?)] = []
        for item in savedForecast {
            _forecast.append((title: item.title, temp: (item.temp == -1000) ? nil : item.temp))
        }
        
        self.savedForecast = _forecast
        
        let savedCurrentWeather = realm.objects(CurrentWeatherItemRealm.self)
        if (!savedCurrentWeather.isEmpty) {
            self.savedWeather = savedCurrentWeather.first!.temp
            self.savedCity = savedCurrentWeather.first!.city
        }
    }
    
    func saveWeather() {

        let realm = try! Realm()
        let savedForecast = realm.objects(ForecastItemRealm.self)
        
        let newForecastList = List<ForecastItemRealm>()
        for item in forecast {
            newForecastList.append(ForecastItemRealm().new( item.title, item.temp) )
        }
        
        let newCurrentWeather = CurrentWeatherItemRealm()
        newCurrentWeather.city = self.city
        newCurrentWeather.temp = self.weather
        
        let savedCurrentWeather = realm.objects(CurrentWeatherItemRealm.self)
        
        try! realm.write {
            realm.delete(savedForecast)
            realm.delete(savedCurrentWeather)
            realm.add(newForecastList)
            realm.add(newCurrentWeather)
        }

    }
    
    func prepareForecast(_ array: [(date: Date, temp: Int)]) {
        forecast = []

        var i = 0
        while (i != Int(Calendar.current.component(.hour, from: array[0].date))) {
            forecast.append(("Сегодня".uppercased(), nil))
            i += 3
        }
        
        for item in array {
            let today = Int(Calendar.current.component(.day, from: Date()))
            let day = Int(Calendar.current.component(.day, from: item.date))
            let month = getMonthName(Int(Calendar.current.component(.month, from: item.date)))

            var title = "\(day) \(month)"
            if (day == today) { title = "Сегодня" }
            if (day - today == 1) { title = "Завтра" }
            forecast.append((title.uppercased(), item.temp))
        }
    }
    
    func getMonthName(_ n: Int) -> String {
        switch n {
        case 1:  return "Января"
        case 2:  return "Февраля"
        case 3:  return "Марта"
        case 4:  return "Апреля"
        case 5:  return "Мая"
        case 6:  return "Июня"
        case 7:  return "Июля"
        case 8:  return "Августа"
        case 9:  return "Сентября"
        case 10: return "Октября"
        case 11: return "Ноября"
        default: return "Декабря"
        }
    }
    
    func parceForecastJSON(_ json: NSDictionary) {
        var forecast: [(date: Date, temp: Int)] = []
        if let list = json["list"] as? NSArray {
            for _item in list {
                if let item = _item as? NSDictionary,
                    let main = item["main"] as? NSDictionary,
                        let temp = main["temp"] as? Double,
                            let dt = item["dt"] as? Double {
                                let date = NSDate(timeIntervalSince1970: dt) as Date
                                forecast.append((date, Int(temp)))
                }
            }
            self.prepareForecast(forecast)
            self.forecastUpdated = true
        }
        else { error() }
    }
    
    func parceWeatherJSON(_ json: NSDictionary) {
        if let main = json["main"] as? NSDictionary,
            let temp = main["temp"] as? Double {
                self.weather = Int(temp)
                self.weatherUpdated = true
        }
        else { error() }
    }

    func getWeather(type: String) {
        let apikey = "a86163a5a4fc01be5bdf8f2476bcbd1f"
        let urlString = "http://api.openweathermap.org/data/2.5/\(type)?q=\(city)&APPID=\(apikey)&units=metric"
        Alamofire.request(urlString).responseJSON { response in
            if (response.error == nil) {
                if let jsonData = response.result.value as? NSDictionary {
                    if (type == "forecast") { self.parceForecastJSON(jsonData) }
                    if (type == "weather") { self.parceWeatherJSON(jsonData) }
                    self.checkUpdated()
                }
            }
            else { self.error() }
        }
    }
    
    func update() {
        weatherUpdated = false
        getWeather(type: "weather")
        forecastUpdated = false
        getWeather(type: "forecast")
    }
    
    func checkUpdated() {
        if (weatherUpdated && forecastUpdated) {
            DispatchQueue.main.async {
                self.newWeatherloaded = true
                self.saveWeather()
                self.delegate?.updated(message: "Updated", sender: self)
            }
        }
    }
    
    func error() {
        DispatchQueue.main.async {
            self.delegate?.updated(message: "Error", sender: self)
        }
    }
}
