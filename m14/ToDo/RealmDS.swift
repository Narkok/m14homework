import Foundation
import RealmSwift

class TodoListItem: Object {
    @objc dynamic var task: String = ""
    @objc dynamic var done: Bool = false
    
    func new(_ task: String, _ done: Bool) -> TodoListItem {
        self.task = task
        self.done = done
        return self
    }
    
}

class StoreUsingRealm {
    static let shared = StoreUsingRealm()
    private let realm = try! Realm()
    
    var realmTodoList = List<TodoListItem>()
    
    func save(list: [(task: String, done: Bool)]) {
        
        realmTodoList.removeAll()
        for item in list {
            realmTodoList.append( TodoListItem().new(item.task, item.done) )
        }
        
        let savedLists = realm.objects(TodoListItem.self)
        try! realm.write {
            realm.delete(savedLists)
            realm.add(realmTodoList)
        }
    }
    
    func get() -> [(task: String, done: Bool)] {
        
        let savedLists = realm.objects(TodoListItem.self)
        var todoList: [(task: String, done: Bool)] = []
        
        for item in savedLists {
            todoList.append((task: item.task, done: item.done))
        }
        
        return todoList
    }
    
    func clearDB() {
        let savedLists = realm.objects(TodoListItem.self)
        try! realm.write {
            realm.delete(savedLists)
        }
    }
    
    
}
