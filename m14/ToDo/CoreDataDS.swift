import Foundation
import CoreData

final class StoreUsingCoreData {
    
    private init() {}
    static let shared = StoreUsingCoreData()
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "m14")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    lazy var context = persistentContainer.viewContext
    
    // MARK: - Core Data Saving support
    
    func save(list: [(title: String, done: Bool)]) {
        
        guard let coreDatalist = try! StoreUsingCoreData.shared.context.fetch(Task.fetchRequest()) as? [Task]
            else { return }
        
        coreDatalist.forEach { (task) in
            StoreUsingCoreData.shared.context.delete(task)
        }
        
        list.forEach { (title: String, done: Bool) in
            let newTask = Task(context: StoreUsingCoreData.shared.context)
            newTask.title = title
            newTask.done = done
            StoreUsingCoreData.shared.save()
        }
    }
    
    func get() -> [(title: String, done: Bool)] {
        
        var list: [(title: String, done: Bool)] = []
        
        guard let coreDatalist = try! StoreUsingCoreData.shared.context.fetch(Task.fetchRequest()) as? [Task]
            else { return [] }
        
        coreDatalist.forEach { (task) in
            let newTask = (task.title, task.done)
            list.append(newTask)
        }
        
        return list
    }
    
    func save() {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
}
