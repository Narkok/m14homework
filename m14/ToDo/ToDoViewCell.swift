import UIKit

class ToDoViewCell: UITableViewCell {

    @IBOutlet weak var line: UIView!
    @IBOutlet weak var task: UILabel!
    
    func set(task: String, done: Bool, index: Int) {
        
        self.task.text = task
        line.backgroundColor = UIColor.darkGray

        if (index % 2 == 1) { backgroundColor = UIColor.white }
        else { backgroundColor = nil }
        
        if (done) {
            accessoryType = .checkmark
            line.isHidden = false
            self.task.textColor = UIColor.lightGray
        }
        else {
            accessoryType = .none
            line.isHidden = true
            self.task.textColor = UIColor.darkGray
        }
        
    }
}
