import UIKit

enum DataStoreType {
    case Realm, CoreData
}

class ToDoListVC: UIViewController {

    @IBOutlet weak var dataStorageSelector: UISegmentedControl!
    @IBOutlet weak var textInputView: UIView!
    @IBOutlet weak var textInputField: UITextField!
    @IBOutlet weak var textInputViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var todoListTableView: UITableView!
    
    @IBOutlet weak var emtpyToDoListLabel: UILabel!
    
    var dataStoreType = DataStoreType.Realm
    
    var todoList: [(String, Bool)] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getData()
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillShow),name: UIResponder.keyboardWillShowNotification,object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillHide),name: UIResponder.keyboardWillHideNotification,object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func saveData() {
        
        if (dataStoreType == .Realm) {
            StoreUsingRealm.shared.save(list: todoList)
        }
        
        if (dataStoreType == .CoreData) {
            StoreUsingCoreData.shared.save(list: todoList)
        }
    }
    
    func getData() {
        if (dataStoreType == .Realm) {
            todoList = StoreUsingRealm.shared.get()
        }
        
        if (dataStoreType == .CoreData) {
            todoList = StoreUsingCoreData.shared.get()
        }
        
        todoListTableView.reloadData()
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            let tabBarHeight = tabBarController!.tabBar.frame.size.height
            textInputViewBottomConstraint.constant = keyboardHeight - tabBarHeight
            UIView.animate(withDuration: 0, animations: { self.view.layoutIfNeeded() })
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        textInputViewBottomConstraint.constant = 0
        UIView.animate(withDuration: 0, animations: { self.view.layoutIfNeeded() })
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.view.endEditing(true)
    }
    
    @IBAction func addButton(_ sender: Any) {
        if (textInputField.text == "") { return }
        let task = (textInputField.text!, false)
        todoList.insert(task, at: 0)
        textInputField.text = ""
        todoListTableView.reloadData()
        saveData()
    }
    
    @IBAction func dataStorageTypeChanged(_ sender: Any) {
        if (dataStorageSelector.selectedSegmentIndex == 1) { dataStoreType = .CoreData }
        else { dataStoreType = .Realm }
        getData()
    }
    
}

extension ToDoListVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        emtpyToDoListLabel.isHidden = todoList.isEmpty ? false : true
        return todoList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ToDoTaskCell") as! ToDoViewCell
        cell.set(task: todoList[indexPath.row].0, done: todoList[indexPath.row].1, index: indexPath.row)
        return cell
    }
}

extension ToDoListVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        self.view.endEditing(true)
        
        todoList[indexPath.row].1 = !todoList[indexPath.row].1
        let cell = tableView.cellForRow(at: indexPath)! as! ToDoViewCell
        cell.set(task: todoList[indexPath.row].0, done: todoList[indexPath.row].1, index: indexPath.row)
        
        saveData()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            todoList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            saveData()
        }
    }
}
